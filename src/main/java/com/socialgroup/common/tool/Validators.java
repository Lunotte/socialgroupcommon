package com.socialgroup.common.tool;

import java.util.Collection;

import com.socialgroup.common.exception.SgInvalidArgumentException;
import com.socialgroup.common.exception.SgRulesException;

public class Validators {

	private Validators() {}

	public static void isNull(final Object object, final String message) throws SgInvalidArgumentException {
		if(object == null) {
			throw new SgInvalidArgumentException(message);
		}
	}

	public static void isNullOrEmpty(final Object object, final String message) throws SgInvalidArgumentException {
		if(object == null || object == "") {
			throw new SgInvalidArgumentException(message);
		}
	}

	public static void isNullOrEmptyAndCheckCondition(final Object object, final boolean condition, final String message) throws SgInvalidArgumentException, SgRulesException {
		isNullOrEmpty(object, message);
		checkCondition(condition, message);
	}

	public static boolean isNotNullAndNotEmptySoCheckCondition(final Object object, final boolean condition, final String message) throws SgRulesException {
		if(object != null && object != "") {
			return checkBooleanCondition(condition, message);
		} else {
			return false;
		}
	}

	public static void isEmpty(final Collection<?> object, final String message) throws SgInvalidArgumentException {
		if(object == null || object.isEmpty()) {
			throw new SgInvalidArgumentException(message);
		}
	}

	public static void isEmptyAndCheckCondition(final Collection<?> object, final boolean condition, final String message) throws SgInvalidArgumentException, SgRulesException {
		isEmpty(object, message);
		checkCondition(condition, message);
	}

	public static void checkCondition(final boolean condition, final String message) throws SgRulesException {
		if(!condition) {
			throw new SgRulesException(message);
		}
	}

	public static boolean checkBooleanCondition(final boolean condition, final String message) throws SgRulesException {
		if(!condition) {
			return false;
		}
		return true;
	}

	public static void compareObjectIsNotEquals(final Object object1, final Object object2, final String message) throws SgRulesException {
		if(!object1.equals(object2)) {
			throw new SgRulesException(message);
		}
	}

}
