package com.socialgroup.common.exception;

import javax.persistence.PersistenceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SgManagerExceptionRouter {

	private static final Logger LOGGER = LoggerFactory.getLogger(SgManagerExceptionRouter.class);

	private SgManagerExceptionRouter() {
	}

	public static SgException throwException(final String message, final Exception ex)
	{
		LOGGER.debug("Erreur remontée depuis les services");

		if (ex instanceof SgException)
	      {
			((SgException) ex).setMessageException("\n" + message);
	         return (SgException) ex;
	      }
	      else if (ex instanceof PersistenceException)
	      {
	         return new SgException(EnumException.BDD);
	      }
	      else
	      {
	         return new SgException(EnumException.AUTRE);
	      }
	}

}
