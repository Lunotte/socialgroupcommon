package com.socialgroup.common.exception;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

class SgExceptionResponse {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private final ZonedDateTime timestamp;
    private final int status;
    private final String error;

	public SgExceptionResponse(final ZonedDateTime timestamp, final int status, final String error) {
		this.timestamp = timestamp;
		this.status = status;
		this.error = error;
	}

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

	public int getStatus() {
		return status;
	}

	public String getError() {
		return error;
	}

}