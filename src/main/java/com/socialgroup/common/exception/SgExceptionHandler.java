package com.socialgroup.common.exception;

import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class SgExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(SgExceptionHandler.class);

	@ExceptionHandler({SgItemNotFoundException.class, SgInvalidArgumentException.class, SgRulesException.class})
    public ResponseEntity<SgExceptionResponse> customHandleSgException(final SgMasterException ex, final WebRequest request) {
		LOG.debug(ex.getMessage(), ex);
		SgExceptionResponse errors = null;
		HttpStatus status = null;

		switch (ex.getEnumException()) {
			case ITEM_NOT_FOUND :
				errors = new SgExceptionResponse(ZonedDateTime.now(), HttpStatus.NOT_FOUND.value(), ex.getCause().getMessage());
				status = HttpStatus.NOT_FOUND;
				break;
			case INVALID_ARGUMENT :
			case RULES :
				errors = new SgExceptionResponse(ZonedDateTime.now(), HttpStatus.BAD_REQUEST.value(), ex.getCause().getMessage());
				status = HttpStatus.BAD_REQUEST;
				break;
			default :
				errors = new SgExceptionResponse(ZonedDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.value(), notManaged(ex.getCause().getMessage()));
				status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
        return new ResponseEntity<>(errors, status);

    }

	@ExceptionHandler({Exception.class})
    public ResponseEntity<SgExceptionResponse> customHandleOtherException(final Exception ex, final WebRequest request) {
		LOG.debug(ex.getMessage(), ex);

        final SgExceptionResponse errors = new SgExceptionResponse(ZonedDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.value(), "Erreur interne non gérée");

        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);

    }

	private String notManaged(final String message) {
		return message == null ? "Erreur interne non gérée" : message;
	}


}
