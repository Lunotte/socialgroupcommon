package com.socialgroup.common.exception;

public class SgItemNotFoundException extends SgException{

	private static final long serialVersionUID = 6249640323014615510L;

	public SgItemNotFoundException(final String message) {
		super(message, EnumException.ITEM_NOT_FOUND);
	}

	public SgItemNotFoundException(final String message, final Throwable cause) {
        super(message, cause, EnumException.ITEM_NOT_FOUND);
    }
}
