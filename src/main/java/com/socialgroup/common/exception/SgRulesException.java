package com.socialgroup.common.exception;

public class SgRulesException extends SgException{

	private static final long serialVersionUID = 6249640323014615510L;

	public SgRulesException(final String message) {
		super(message, EnumException.RULES);
	}

	public SgRulesException(final String message, final Throwable cause) {
        super(message, cause, EnumException.RULES);
    }
}
