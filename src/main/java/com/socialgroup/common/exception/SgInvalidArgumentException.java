package com.socialgroup.common.exception;

public class SgInvalidArgumentException extends SgException{

	private static final long serialVersionUID = 6249640323014615510L;

	public SgInvalidArgumentException(final String message) {
		super(message, EnumException.INVALID_ARGUMENT);
	}

	public SgInvalidArgumentException(final String message, final Throwable cause) {
        super(message, cause, EnumException.INVALID_ARGUMENT);
    }

}
