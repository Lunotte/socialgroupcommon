package com.socialgroup.common.exception;

public class SgException extends SgMasterException{

	private static final long serialVersionUID = 6249640323014615510L;

	private final EnumException enumException;

	public SgException() {
		super();
		this.enumException = null;
		this.messageException = null;
	}

	public SgException(final EnumException enumException) {
		super();
		this.enumException = enumException;
		this.messageException = null;
	}

	public SgException(final String message) {
		super(message);
		this.messageException = message;
		this.enumException = null;
	}

	public SgException(final String message, final EnumException enumException) {
		super(message);
		this.enumException = enumException;
		this.messageException = message;
	}

	public SgException(final Throwable cause) {
        super(cause);
        this.enumException = null;
		this.messageException = null;
	}

	public SgException(final String message, final Throwable cause) {
        super(message, cause);
        this.enumException = null;
		this.messageException = message; // Pas certain
    }

	public SgException(final String message, final Throwable cause, final EnumException enumException) {
        super(message, cause);
        this.enumException = enumException;
		this.messageException = message;
    }


	@Override
	public EnumException getEnumException() {
		return enumException;
	}

	@Override
	public String getMessageException() {
		return messageException;
	}

	@Override
	public void setMessageException(final String messageException) {
		this.messageException = messageException;
	}


//	public static SgException wrappe(final Throwable cause) throws SgException {
//		if(cause instanceof PersistenceException) {
//			return new SgException(ERROR_IN + EnumException.DAO, cause);
//		} else {
//			return new SgException(ERROR_IN + EnumException.AUTRE, cause);
//		}
//	}
//
//	public static SgException wrappe(final String message, final Throwable cause) throws SgException {
//		if(cause instanceof PersistenceException) {
//			return new SgException(ERROR_IN + EnumException.DAO + "\n" + message, cause);
//		}
//		else if(cause instanceof SgInvalidArgumentException) {
//			return new SgInvalidArgumentException(ERROR_IN + EnumException.INVALID_ARGUMENT + "\n" + message, cause);
//		}
//		else if(cause instanceof SgItemNotFoundException) {
//			return new SgException(ERROR_IN + EnumException.ITEM_NOT_FOUND + "\n" + message, cause);
//		}else {
//			return new SgException(ERROR_IN + EnumException.AUTRE, cause);
//		}
//	}

}