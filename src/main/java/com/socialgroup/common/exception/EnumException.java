package com.socialgroup.common.exception;

public enum EnumException {
	BDD,
	AUTRE,
	INVALID_ARGUMENT,
	ITEM_NOT_FOUND,
	RULES
}
