package com.socialgroup.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class SgMasterManagerExceptionRouter {

	private static final Logger LOGGER = LoggerFactory.getLogger(SgMasterManagerExceptionRouter.class);

	private SgMasterManagerExceptionRouter(){

	}

	public static SgMasterException throwException(final String message, final Exception ex)
	{
		LOGGER.debug("Une erreur s'est produite (Facade):", ex);
		if (ex instanceof SgException)
		   {
		      final SgMasterException sgEx = (SgMasterException) ex;
		      sgEx.setMessageException(sgEx.getMessageException() == null ? message : sgEx.getMessageException() + "\n" + message);

		      switch (sgEx.getEnumException())
		      {
		         case ITEM_NOT_FOUND:
		            return new SgItemNotFoundException(sgEx.getMessageException(), ex);
		         case INVALID_ARGUMENT:
		            return new SgInvalidArgumentException(sgEx.getMessageException(), ex);
		         case RULES:
		        	  return new SgRulesException(sgEx.getMessageException(), ex);
		         case BDD:
		        	 return new SgBDDException();
		         case AUTRE:
		        	 return new SgInconnuException();
		         default:
		        	 return new SgInconnuException();
		      }
		   }
		   else
		   {
			   return new SgInconnuException(ex);
		   }
   }
}

