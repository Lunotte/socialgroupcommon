package com.socialgroup.common.exception;

public class SgMasterException extends Exception{

	private static final long serialVersionUID = 6249640323014615510L;

	protected final EnumException enumException;
	protected String messageException;

	public SgMasterException() {
		super();
		this.enumException = null;
		this.messageException = null;
	}

	public SgMasterException(final EnumException enumException) {
		super();
		this.enumException = enumException;
		this.messageException = null;
	}

	public SgMasterException(final String message) {
		super(message);
		this.enumException = null;
		this.messageException = message;
	}

	public SgMasterException(final String message, final EnumException enumException) {
		super(message);
		this.enumException = enumException;
		this.messageException = message;
	}

	public SgMasterException(final Throwable cause) {
        super(cause);
        this.enumException = null;
		this.messageException = cause.getMessage(); // Pas certain
	}

	public SgMasterException(final String message, final Throwable cause) {
        super(message, cause);
        this.enumException = null;
		this.messageException = null;
    }


	public EnumException getEnumException() {
		return enumException;
	}

	public String getMessageException() {
		return messageException;
	}

	public void setMessageException(final String messageException) {
		this.messageException = messageException;
	}

}