package com.socialgroup.common.exception;

public class SgInconnuException extends SgException{

	private static final long serialVersionUID = 6249640323014615510L;

	public SgInconnuException() {
		super();
	}

	public SgInconnuException(final Throwable cause) {
        super(cause);
    }

	public SgInconnuException(final String message, final Throwable cause) {
        super(message, cause, EnumException.AUTRE);
    }
}
